import { AuthenticationStoreState } from 'redux/Authentication';

import { ProtectedRouteStateProps } from './component';

export const mapStateToProps = (state: AuthenticationStoreState): ProtectedRouteStateProps => {
  return {
    authenticated: state.authenticated,
  };
};
