import { connect } from 'react-redux';
import { createSelector } from 'reselect';
import { compose } from 'recompose';

import { authenticationStateSelector } from 'redux/selectors';

import { mapStateToProps } from './redux';
import { ProtectedRouteInnerProps, ProtectedRouteProps, ProtectedRouteComponent } from './component';

export const ProtectedRoute = compose<ProtectedRouteInnerProps, ProtectedRouteProps>(
  connect(createSelector(authenticationStateSelector, mapStateToProps))
)(ProtectedRouteComponent);
