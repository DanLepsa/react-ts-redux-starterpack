import React from 'react';
import { Redirect, RouteProps, Route, RouteComponentProps } from 'react-router-dom';

export interface ProtectedRouteStateProps {
  authenticated: boolean;
}

export interface ProtectedRouteProps extends RouteProps {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  component: React.ComponentType<RouteComponentProps<any>> | React.ComponentType<any>;
}

export interface ProtectedRouteInnerProps extends ProtectedRouteProps, ProtectedRouteStateProps {}

export const ProtectedRouteComponent = ({
  authenticated,
  component: Component,
  ...restProps
}: ProtectedRouteInnerProps) => {
  return (
    <Route
      {...restProps}
      render={(props) =>
        authenticated ? (
          <Component {...props} />
        ) : (
          <Redirect
            to={{
              pathname: '/login',
            }}
          />
        )
      }
    />
  );
};
