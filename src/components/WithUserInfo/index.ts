import { connect } from 'react-redux';
import { createSelector } from 'reselect';
import { compose } from 'recompose';

import { authenticationStateSelector } from 'redux/selectors';

import { mapStateToProps } from './redux';
import { InjectedUserInfoInnerProps, InjectedUserInfoProps as IUIP, withUserInfoHOC } from './component';

export const withUserInfo = compose<InjectedUserInfoInnerProps, InjectedUserInfoProps>(
  connect(createSelector(authenticationStateSelector, mapStateToProps)),
  withUserInfoHOC
);

export type InjectedUserInfoProps = IUIP;
