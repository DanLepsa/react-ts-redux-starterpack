import React from 'react';
import { Subtract } from 'utility-types';

export interface WithUserInfoStateProps {
  authenticated: boolean;
  accessToken?: string;
}

export interface InjectedUserInfoProps {
  authenticated: boolean;
  accessToken?: string;
}

export interface InjectedUserInfoInnerProps extends InjectedUserInfoProps, WithUserInfoStateProps {}

export const withUserInfoHOC = <P extends InjectedUserInfoProps>(Component: React.ComponentType<P>) =>
  class WithUserInfo extends React.Component<Subtract<P, InjectedUserInfoInnerProps>> {
    render() {
      return <Component {...(this.props as P)} />;
    }
  };
