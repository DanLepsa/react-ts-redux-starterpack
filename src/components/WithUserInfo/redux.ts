import { AuthenticationStoreState } from 'redux/Authentication';

import { WithUserInfoStateProps } from './component';

export const mapStateToProps = (state: AuthenticationStoreState): WithUserInfoStateProps => {
  return {
    authenticated: state.authenticated,
    accessToken: state.accessToken,
  };
};
