import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { ThemeProvider, CssBaseline } from '@material-ui/core';
import { PersistGate } from 'redux-persist/integration/react';

import { configureStore } from 'redux/store';
import { authenticationStateSelector } from 'redux/selectors';
import { theme } from 'theme';
import { loginExpired } from 'helpers';

import App from './App';
import * as serviceWorker from './serviceWorker';

import './language/i18n';

const { store, persistor } = configureStore();

store.subscribe(async () => {
  const state = store.getState();

  const authData = authenticationStateSelector(state);

  if (authData.authenticated && authData.expiresAt) {
    if (loginExpired(authData.expiresAt)) {
      // delete persisted info
      // reset auth reducer
      await persistor.purge();
    }
  }
});

ReactDOM.render(
  <Provider store={store}>
    <ThemeProvider theme={theme}>
      <CssBaseline>
        <PersistGate loading={null} persistor={persistor}>
          <App />
        </PersistGate>
      </CssBaseline>
    </ThemeProvider>
  </Provider>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
