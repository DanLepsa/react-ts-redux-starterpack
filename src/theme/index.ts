import { createMuiTheme, ThemeOptions } from '@material-ui/core';

const createTheme = () => {
  const options: ThemeOptions = {};

  return createMuiTheme(options);
};

export const theme = createTheme();
