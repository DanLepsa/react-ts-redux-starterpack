import { createAsyncAction, createAction } from 'typesafe-actions';

import { Post } from 'api/FE';

import { ActionTypes } from './actionTypes';

export const getPostsAction = createAsyncAction(
  ActionTypes.GET_POSTS_PENDING,
  ActionTypes.GET_POSTS_SUCCESS,
  ActionTypes.GET_POSTS_ERROR
)<undefined, Post[], Error>();
