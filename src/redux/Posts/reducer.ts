import { ActionType, getType } from 'typesafe-actions';

import { Post } from 'api/FE';

import * as Actions from './actionCreators';

export type PostsAction = ActionType<typeof Actions>;

export interface PostsStoreState {
  posts: Post[];
  pending: boolean;
  error: boolean;
}

export const initialStoreState: PostsStoreState = {
  posts: [],
  pending: false,
  error: false,
};

export const reducer = (state: PostsStoreState = initialStoreState, action: PostsAction): PostsStoreState => {
  switch (action.type) {
    case getType(Actions.getPostsAction.request):
      return {
        ...state,
        pending: true,
      };

    case getType(Actions.getPostsAction.success):
      return {
        ...state,
        posts: action.payload,
        pending: false,
        error: false,
      };

    case getType(Actions.getPostsAction.failure):
      return {
        ...state,
        pending: false,
        error: true,
      };

    default:
      return state;
  }
};
