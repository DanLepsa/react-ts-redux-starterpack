import { Dispatch } from 'redux';

import { getPostsMock as getPostsFromApi } from 'mocks';
import { Post } from 'api/FE';

import { getPostsAction } from './actionCreators';

export const getPosts = () => (dispatch: Dispatch) => {
  dispatch(getPostsAction.request());

  getPostsFromApi()
    .then((response: Post[]) => {
      dispatch(getPostsAction.success(response));
    })
    .catch((err: Error) => {
      dispatch(getPostsAction.failure(err));
    });
};
