import { combineReducers } from 'redux';
import { persistReducer, PersistConfig } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import { PersistPartial } from 'redux-persist/lib/persistReducer';

import { reducer as AuthenticationReducer, AuthenticationStoreState } from './Authentication/reducer';
import { reducer as PostsReducer, PostsStoreState } from './Posts/reducer';

export interface AppStoreState {
  authentication: AuthenticationStoreState & PersistPartial;
  posts: PostsStoreState;
}

const authPersistConfig: PersistConfig<AuthenticationStoreState> = {
  key: 'authentication',
  storage,
  whitelist: ['authenticated', 'accessToken', 'expiresAt'],
};

export const rootReducer = combineReducers<AppStoreState>({
  authentication: persistReducer(authPersistConfig, AuthenticationReducer),
  posts: PostsReducer,
});
