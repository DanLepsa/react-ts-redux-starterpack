import { ActionType, getType } from 'typesafe-actions';

import * as Actions from './actionCreators';

export type AuthenticationAction = ActionType<typeof Actions>;

export interface AuthenticationStoreState {
  authenticated: boolean;
  accessToken?: string;
  expiresIn?: number;
  expiresAt?: Date;
  pending: boolean;
  error: boolean;
}

export const initialStoreState: AuthenticationStoreState = {
  authenticated: false,
  accessToken: undefined,
  expiresIn: undefined,
  expiresAt: undefined,
  pending: false,
  error: false,
};

export const reducer = (
  state: AuthenticationStoreState = initialStoreState,
  action: AuthenticationAction
): AuthenticationStoreState => {
  switch (action.type) {
    case getType(Actions.loginAction.request):
      return {
        ...state,
        pending: true,
      };

    case getType(Actions.loginAction.success):
      return {
        ...state,
        authenticated: true,
        accessToken: action.payload.accessToken,
        expiresIn: action.payload.expiresIn,
        expiresAt: action.payload.expiresAt,
        pending: false,
        error: false,
      };

    case getType(Actions.loginAction.failure):
      return {
        ...state,
        pending: false,
        error: true,
      };

    case getType(Actions.flushStoreAction): {
      return initialStoreState;
    }

    case getType(Actions.purgeStoreAction): {
      return initialStoreState;
    }

    default:
      return state;
  }
};
