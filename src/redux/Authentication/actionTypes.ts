export enum ActionTypes {
  LOGIN_PENDING = 'LOGIN_PENDING',
  LOGIN_SUCCESS = 'LOGIN_SUCCESS',
  LOGIN_ERROR = 'LOGIN_ERROR',
  FLUSH = 'persist/FLUSH',
  PURGE = 'persist/PURGE',
  LOGOUT = 'LOGOUT',
}
