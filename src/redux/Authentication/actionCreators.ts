import { createAsyncAction, createAction } from 'typesafe-actions';

import { LoginResponseFormattedData } from 'api/FE';

import { ActionTypes } from './actionTypes';

export const loginAction = createAsyncAction(
  ActionTypes.LOGIN_PENDING,
  ActionTypes.LOGIN_SUCCESS,
  ActionTypes.LOGIN_ERROR
)<undefined, LoginResponseFormattedData, Error>();

export const flushStoreAction = createAction(ActionTypes.FLUSH)();
export const purgeStoreAction = createAction(ActionTypes.PURGE)();
export const logoutAction = createAction(ActionTypes.LOGOUT)();
