import { Dispatch } from 'redux';
import moment from 'moment';

import { loginUserMock as loginUser } from 'mocks';
import { LoginResponseData, LoginResponseFormattedData, LoginCredentials } from 'api/FE';

import { loginAction, logoutAction } from './actionCreators';

export const login = (credentials: LoginCredentials) => (dispatch: Dispatch) => {
  dispatch(loginAction.request());

  loginUser(credentials)
    .then((response: LoginResponseData) => {
      const formattedResponse: LoginResponseFormattedData = {
        accessToken: response.accessToken,
        expiresIn: response.expires_in,
        expiresAt: moment(new Date())
          .add(response.expires_in, 'seconds')
          .toDate(),
      };
      dispatch(loginAction.success(formattedResponse));
    })
    .catch((err: Error) => {
      dispatch(loginAction.failure(err));
    });
};

export const logout = () => (dispatch: Dispatch) => {
  dispatch(logoutAction());
};
