import { applyMiddleware, createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension/developmentOnly';
import thunk from 'redux-thunk';
import { persistStore } from 'redux-persist';

import { rootReducer } from './rootReducer';

const enhancer = composeWithDevTools({})(applyMiddleware(thunk));

const store = createStore(rootReducer, enhancer);
const persistor = persistStore(store);

export const configureStore = () => {
  return { store, persistor };
};

export type AppDispatch = typeof store.dispatch;
