import { AppStoreState } from './rootReducer';

export const authenticationStateSelector = (state: AppStoreState) => state.authentication;

export const postsStateSelector = (state: AppStoreState) => state.posts;
