import i18n, { Resource } from 'i18next';
import LanguageDetector from 'i18next-browser-languagedetector';
import { initReactI18next } from 'react-i18next';

const resources: Resource = {
  en: {
    translation: require('./en/translation.json'),
  },
};

i18n
  .use(LanguageDetector)
  .use(initReactI18next)
  .init({
    resources,
    fallbackLng: 'en',
    whitelist: ['en'],
    interpolation: {
      escapeValue: false,
    },
  });

export const changeLanguage = i18n.changeLanguage;
