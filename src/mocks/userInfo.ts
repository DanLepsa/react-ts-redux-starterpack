import { InjectedUserInfoProps } from 'components/WithUserInfo';

export function getMockInjectedUserInfoProps(): InjectedUserInfoProps {
  return {
    authenticated: false,
    accessToken: '#123%456',
  };
}
