import Chance from 'chance';

import { LoginCredentials, LoginResponseData } from 'api/FE';

export const loginUserMock = async (credentials: LoginCredentials): Promise<LoginResponseData> => {
  await new Promise((r) => setTimeout(r, 500));

  if (credentials.password.length < 3) {
    throw new Error('Mocked login failed');
  }

  const responseData: LoginResponseData = {
    accessToken: Chance().guid(),
    expires_in: Chance().integer({ min: 9600 }),
  };

  return responseData;
};
