import { Post } from 'api/FE';

export const getPostsMock = async (): Promise<Post[]> => {
  const response = await fetch('https://jsonplaceholder.typicode.com/posts');

  const responseData: Post[] = await response.json();
  return responseData;
};
