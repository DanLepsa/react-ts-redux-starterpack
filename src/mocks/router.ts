import { RouteComponentProps } from 'react-router';

export function getMockRouterProps<P>(data: P): RouteComponentProps<P> {
  return {
    match: {
      isExact: true,
      params: data,
      path: '',
      url: '',
    },
    location: {
      hash: '',
      key: '',
      pathname: '',
      search: '',
      state: {},
    },
    history: {
      length: 2,
      action: 'POP',
      location: {
        hash: '',
        key: '',
        pathname: '',
        search: '',
        state: {},
      },
      push: jest.fn(),
      replace: jest.fn(),
      go: jest.fn(),
      goBack: jest.fn(),
      goForward: jest.fn(),
      block: jest.fn(),
      createHref: jest.fn(),
      listen: jest.fn(),
    },
    staticContext: {},
  };
}
