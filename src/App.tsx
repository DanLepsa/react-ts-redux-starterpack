import React from 'react';
import { Route, Router, Switch, Redirect } from 'react-router-dom';
import { createBrowserHistory } from 'history';

import { HomePage, LoginPage } from 'pages';
import { ProtectedRoute } from 'components';
import { Paths } from 'paths';

function App() {
  const history = createBrowserHistory();

  return (
    <div className="App">
      <Router history={history}>
        <Switch>
          <Route path={Paths.LOGIN} component={LoginPage} />
          <ProtectedRoute exact path={Paths.HOME} component={HomePage} />
          <Route exact={true} to={Paths.HOME} render={() => <Redirect to={'/'} />} />
        </Switch>
      </Router>
    </div>
  );
}

export default App;
