import { Dispatch } from 'redux';

import { AuthenticationStoreState, login } from 'redux/Authentication';
import { LoginCredentials } from 'api/FE';

import { LoginDispatchProps, LoginStateProps } from './component';

export const mapStateToProps = (state: AuthenticationStoreState): LoginStateProps => {
  return {
    accessToken: state.accessToken,
    authenticated: state.authenticated,
    pending: state.pending,
    error: state.error,
  };
};

export const mapDispatchToProps = (dispatch: Dispatch): LoginDispatchProps => {
  return {
    login: (credentials: LoginCredentials) => {
      dispatch(login(credentials));
    },
  };
};
