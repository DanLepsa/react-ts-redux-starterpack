import { compose, setDisplayName } from 'recompose';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { createSelector } from 'reselect';
import { withTranslation } from 'react-i18next';
import { withStyles } from '@material-ui/styles';

import { authenticationStateSelector } from 'redux/selectors';

import { LoginPageInnerProps, LoginPageProps, LoginPageComponent } from './component';
import { mapStateToProps, mapDispatchToProps } from './redux';
import { LoginPageStyleRules } from './styles';

export const LoginPage = compose<LoginPageInnerProps, LoginPageProps>(
  setDisplayName('LoginPage'),
  withTranslation(),
  withStyles(LoginPageStyleRules),
  withRouter,
  connect(createSelector(authenticationStateSelector, mapStateToProps), mapDispatchToProps)
)(LoginPageComponent);
