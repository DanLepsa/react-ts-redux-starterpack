import { createStyles, StyleRulesCallback, Theme } from '@material-ui/core';

export type LoginPageClassKeys = 'root';

export const LoginPageStyleRules: StyleRulesCallback<Theme, {}, LoginPageClassKeys> = (theme: Theme) =>
  createStyles({
    root: {
      height: '100vh',
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
    },
  });
