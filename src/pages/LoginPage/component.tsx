import React, { useEffect, useState, ChangeEvent } from 'react';
import { WithTranslation } from 'react-i18next';
import { WithStyles, TextField, Button, Grid, Typography } from '@material-ui/core';
import { RouteComponentProps } from 'react-router-dom';

import { LoginCredentials } from 'api/FE';
import { Paths } from 'paths';
import { validateEmail } from 'helpers';

import { LoginPageClassKeys } from './styles';

export interface LoginPageProps {}

export interface LoginStateProps {
  authenticated: boolean;
  accessToken?: string;
  pending: boolean;
  error: boolean;
}

export interface LoginDispatchProps {
  login: (credentials: LoginCredentials) => void;
}

export interface LoginPageInnerProps
  extends LoginPageProps,
    LoginStateProps,
    LoginDispatchProps,
    WithTranslation,
    WithStyles<LoginPageClassKeys>,
    RouteComponentProps<{}> {}

export const LoginPageComponent = ({
  authenticated,
  accessToken,
  pending,
  error,
  login,
  t,
  classes,
  history,
}: LoginPageInnerProps) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const [emailError, setEmailError] = useState('');
  const [passwordError, setPasswordError] = useState('');

  const handleChangeEmail = (event: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
    setEmail(event.target.value);
  };

  const handleChangePassword = (event: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
    setPassword(event.target.value);
  };

  const handleClickLogin = () => {
    let canProceed = true;

    if (!validateEmail(email)) {
      setEmailError(t('invalid-email'));
      canProceed = false;
    } else {
      setEmailError('');
    }

    if (!password.length) {
      setPasswordError(t('invalid-password'));
      canProceed = false;
    } else {
      setPasswordError('');
    }

    if (canProceed) {
      login({ email, password });
    }
  };

  useEffect(() => {
    if (authenticated) {
      history.push(Paths.HOME);
    }
  }, [authenticated, history]);

  return (
    <div className={classes.root}>
      <div style={{ padding: 20 }}>
        <Grid container direction="column" justify="space-between" alignItems="center" spacing={2}>
          {error && (
            <Grid item xs={12}>
              <Typography variant="body1" component="p" color="error">
                {t('login-failed')}
              </Typography>
            </Grid>
          )}
          <Grid item xs={12}>
            <TextField
              label={t('email')}
              value={email}
              type={'text'}
              onChange={handleChangeEmail}
              error={emailError ? true : false}
              helperText={emailError}
            />
          </Grid>
          <Grid item xs={12}>
            <TextField
              label={t('password')}
              value={password}
              type={'password'}
              onChange={handleChangePassword}
              error={passwordError ? true : false}
              helperText={passwordError}
            />
          </Grid>
          <Grid item xs={12}>
            <Button
              variant="contained"
              color="primary"
              disableElevation={true}
              onClick={handleClickLogin}
              data-id="sign-in"
            >
              {t('sign-in')}
            </Button>
          </Grid>
        </Grid>
      </div>
    </div>
  );
};
