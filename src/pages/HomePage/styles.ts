import { createStyles, Theme, makeStyles } from '@material-ui/core';

// export const styles = (theme: Theme) =>
//   createStyles({
//     root: {
//       height: '100%',
//     },
//     description: {
//       fontFamily: 'Muli',
//       color: theme.palette.primary.light,
//     },
//   });

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      height: '100%',
    },
    description: {
      fontFamily: 'Muli',
      color: theme.palette.primary.light,
    },
  })
);
