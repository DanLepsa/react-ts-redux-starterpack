import { useTranslation, UseTranslationResponse } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';

import { Post } from 'api/FE';
import { PostsStoreState, getPosts as getAllPosts } from 'redux/Posts';
import { AppDispatch } from 'redux/store';
import { postsStateSelector } from 'redux/selectors';

import { useStyles } from './styles';

interface HomePageData {
  posts: Post[];
  getPosts: () => void;
}

export const useData = (): HomePageData => {
  const t = useTranslation();
  const classes = useStyles();
  const dispatch: AppDispatch = useDispatch();

  // const { posts }: PostsStoreState = useSelector((state: PostsStoreState) => state);
  const { posts }: PostsStoreState = useSelector(postsStateSelector);

  const getPosts = () => dispatch(getAllPosts());

  return {
    posts,
    getPosts,
  };
};
