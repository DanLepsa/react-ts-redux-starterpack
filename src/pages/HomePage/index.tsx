import React, { useEffect } from 'react';
import { Typography } from '@material-ui/core';
import { useTranslation } from 'react-i18next';

import { useData } from './useData';
import { useStyles } from './styles';

export interface HomePageProps {}

export const HomePage = ({ ...restProps }: HomePageProps) => {
  const { posts, getPosts } = useData();
  const classes = useStyles();
  const t = useTranslation();

  useEffect(() => {
    getPosts();
  }, []);

  console.log('posts are', posts);
  return (
    <div className={classes.root}>
      <Typography variant="h1">{t.t('home')}</Typography>

      {[...new Array(42)]
        .map(
          () => `Cras mattis consectetur purus sit amet fermentum.
Cras justo odio, dapibus ac facilisis in, egestas eget quam.
Morbi leo risus, porta ac consectetur ac, vestibulum at eros.
Praesent commodo cursus magna, vel scelerisque nisl consectetur et.`
        )
        .join('\n')}
    </div>
  );
};
