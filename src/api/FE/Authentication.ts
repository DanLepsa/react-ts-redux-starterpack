export interface LoginCredentials {
  email: string;
  password: string;
}

export interface LoginResponseData {
  accessToken: string;
  expires_in: number;
}

export interface LoginResponseFormattedData {
  accessToken: string;
  expiresIn: number;
  expiresAt: Date;
}
