import { Chance } from 'chance';
import moment from 'moment';

import { loginExpired } from '../loginExpired';

describe('loginExpired', () => {
  it('returns true when login is valid', () => {
    // Arrange
    let expiresAt = moment(new Date())
      .subtract(Chance().integer({ min: 60, max: 3600 }), 'seconds')
      .toDate();

    // Assert
    expect(loginExpired(expiresAt)).toBeTruthy();
  });

  it('returns false when login expired', () => {
    // Arrange
    let expiresAt = moment(new Date())
      .add(Chance().integer({ min: 60, max: 3600 }), 'seconds')
      .toDate();

    // Assert
    expect(loginExpired(expiresAt)).toBeFalsy();
  });
});
