import { validateEmail } from '../validateEmail';

describe('validateEmail', () => {
  it('returns true when email is valid', () => {
    // Assert
    expect(validateEmail('test@test.com')).toBeTruthy();
  });

  it('returns false when email is not valid', () => {
    // Assert
    expect(validateEmail('notValid.com')).toBeFalsy();
  });
});
