import { verify, decode } from 'jsonwebtoken';

export const decodeToken = async (token: string) => {
  const decoded = await decode(token);

  return decoded;
};

export const verifyToken = async (token: string, secret: string) => {
  try {
    const decoded = await verify(token, secret);

    return decoded;
  } catch (err) {
    // err
    console.log('verify token error ', err);
    throw err;
  }
};
