import moment from 'moment';

export const loginExpired = (expiresAt: Date) => {
  const currentTime = moment(new Date()).format();

  return moment(moment(expiresAt).format()).isBefore(currentTime);
};
