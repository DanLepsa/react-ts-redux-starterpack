import { AnyAction } from 'redux';

declare module 'redux' {
  export interface Dispatch<A extends Action = AnyAction> {
    <T>(fn: (dispatch: Dispatch) => T): T;
  }
}

declare module '*.svg' {
  import React = require('react');
  export const ReactComponent: React.SFC<React.SVGProps<SVGSVGElement>>;
  const src: string;
  export default src;
}
